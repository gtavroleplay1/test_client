using RAGE;
using RAGE.Elements;

namespace client_manager
{
    public class Test : Events.Script
    {
        public Test()
        {
            RAGE.Events.OnOutgoingDamage += OnOutgoingDamage;
        }

        private void OnOutgoingDamage(Entity sourceentity, Entity targetentity, Player sourceplayer, ulong weaponhash, 
            ulong boneidx, int damage, Events.CancelEventArgs cancel)
        {
            if (sourceplayer == Player.LocalPlayer && weaponhash == RAGE.Game.Misc.GetHashKey("weapon_pistol50") &&
                targetentity.Type == Type.Vehicle)
            {
                RAGE.Events.CallRemote("car_paint", targetentity);
            }
        }
    }
}